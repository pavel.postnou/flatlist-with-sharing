export const darkTheme = {
  date: "red",
  header: "yellow",
  country: "#ff6666",
  name: "white",
  back: "black",
  backGround: "#454545",
  describe: "yellow",
  description: "#99b3ff",
  titleName: "#7a6bff",
  titleCountry:"#ff7566",
  image:require("../assets/blackBack.jpg")
};

export const lightTheme = {
  date: "#1f00ff",
  header: "#fa9a1b",
  country: "#ff6666",
  name: "#1bc2fa",
  back: "white",
  backGround: "#c9c9c9",
  describe: "#fa9a1b",
  description:"blue",
  titleName: "#1bc2fa",
  titleCountry:"#ff7566",
  image:require("../assets/lightBack.jpg")
};
