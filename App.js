import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import describeScreen from "./screens/describeScreen";
import errorScreen from "./screens/errorScreen";
import { navigationRef } from "./navigation/RootNavigation";
import mainScreen from "./screens/mainScreen";

const Stack = createStackNavigator();

export default function App() {

  return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator>
          <Stack.Screen
            name="Main"
            component={mainScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen name="Describe" component={describeScreen} />
          <Stack.Screen name="Error" component={errorScreen} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
