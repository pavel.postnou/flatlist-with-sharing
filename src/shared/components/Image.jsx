import React from "react";
import styled from "styled-components";

const AvatarList = styled.Image`
  height: 110px;
  resize-mode: contain;
  border-radius: 5px;
`;

const AvatarDescription = styled.Image`
  width:50%;
  margin-left: 10px;
  resize-mode: contain;
  border-radius:10px;
`;

const ErrorImage = styled.Image`
  width: 100px;
  height: 100px;
  resize-mode: contain;
`;

const Icon = styled.Image`
  width: 30px;
  height: 30px;
  resize-mode: contain;
`;

const RenderImageComponent = ({ type, source, ...rest }) => {
  return type == "imgOne" ? (
    <AvatarList source={source} />
  ) : type == "imgDescription" ? (
    <AvatarDescription source={source} />
  ) : type == "errorImg" ? (
    <ErrorImage source={source} />
  ): type == "icon" ? (
    <Icon source={source} />
  ) : null;
};


export default function ImageComponent({ type, source, ...rest }) {
  return <RenderImageComponent type={type} source={source}/>;
}
