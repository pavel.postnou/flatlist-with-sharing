import * as RootNavigation from "../../navigation/RootNavigation";
import React, { useState, useEffect } from "react";
import Item from "../../components/Item";
import refreshFetch from "../../api/fetchRequest";
import { lightTheme, darkTheme } from "../../theme/theme";
import { Switch, FlatList} from "react-native";
import { AllContainer, SafeContainer, ThemeText, SwitchContainer, Input, Indicator } from "./mainScreen.styled";

export default function mainScreen() {
  let key = 0;
  const [dark, setDark] = useState(true);
  const [theme, setTheme] = useState(lightTheme);
  const [flatListItem, setFlatlistItem] = useState([]);
  const [search, setSearch] = useState("");
  const [users, setUsers] = useState(null);
  const [loading, setLoading] = useState(true);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () =>
    setIsEnabled(
      (previousState) => !previousState,
      setDark(!dark),
      dark ? setTheme(darkTheme) : setTheme(lightTheme)
    );

  async function refresh() {
    setLoading(true);
    setUsers(null);
    const result = await refreshFetch();
    result
      ? (setUsers(result), setLoading(false))
      : (setLoading(false),
        RootNavigation.navigate("Error", { message: "Something went wrong" }));
  }

  useEffect(() => {
    refresh();
  }, []);

  function add() {
    let listItem = [];
    for (let i = 0; i < users.results.length; i++) {
      listItem.push({
        number: key,
        id: users.results[i].login.uuid,
        name: users.results[i].name.first,
        image: users.results[i].picture.large,
        lastName: users.results[i].name.last,
        country: users.results[i].location.country,
        city: users.results[i].location.city,
        date: users.results[i].registered.date.substring(0, 10),
      });
      key++;
    }
    setFlatlistItem(listItem);
  }

  useEffect(() => {
    users ? add() : null;
  }, [users]);

  const renderItem = ({ item }) =>
    (item && item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
    item.lastName.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? (
      <Item data={item} users={users} theme={theme} />
    ) : null;

  return (
    <AllContainer theme={theme}>
      {!loading ? (
        <SafeContainer>
          <Input
            placeholder="Search"
            onChangeText={(text) => setSearch(text)}
            value={search}
          />
          <FlatList
            data={flatListItem}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            refreshing={false}
            onRefresh={refresh}
          />
          <SwitchContainer>
            <Switch
              trackColor={{ false: "white", true: "black" }}
              thumbColor={isEnabled ? "yellow" : "#1bc2fa"}
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </SwitchContainer>
          <ThemeText>{`Switch\ntheme`}</ThemeText>
        </SafeContainer>
      ) : (
        <Indicator size={100} color="red" />
      )}
    </AllContainer>
  );
}
