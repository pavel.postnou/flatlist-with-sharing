import styled from "styled-components/native";

const AllContainer = styled.View`
  margin-top: 45px;
  flex: 1;
  justify-content: center;
  background-color: ${(props) => props.theme.backGround};
`;

const SafeContainer = styled.SafeAreaView`
  flex: 1;
`;
const ThemeText = styled.Text`
  position: absolute;
  top: 12px;
  right: 80px;
  height: 40px;
  font-weight: bold;
  color: red;
`;

const SwitchContainer = styled.View`
  position: absolute;
  border-radius:20px;
  border-color:red;
  border-width:1px;
  width:60px;
  height:100px;
  top: 12px;
  right: 10px;
  height: 40px;
  align-items:center;
  justify-content:center;
  background-color:grey;
`;

const Input = styled.TextInput`
  color: #555555;
  width: 55%;
  margin-bottom: 10px;
  margin-top: 15px;
  margin-left: 15px;
  padding-right: 10px;
  padding-left: 10px;
  border-color: black;
  border-width: 3px;
  border-radius: 5px;
  align-self: flex-start;
  background-color: #ffffff;
`;

const Indicator = styled.ActivityIndicator`
  color: red;
`;

export {AllContainer, SafeContainer, ThemeText, SwitchContainer, Input, Indicator}