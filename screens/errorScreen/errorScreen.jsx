import React from "react";
import ImageComponent from "../../src/shared/components/Image";
import TextComponent from "../../src/shared/components/Text";
import styled from "styled-components/native";

const Container = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content:center;
  align-items: center;
`;
export default function errorScreen() {
  return (
    <Container>
      <ImageComponent
        type="errorImg"
        source={require("../../assets/error.png")}
      />
      <TextComponent color="red" type="title" text="Something went wrong" />
    </Container>
  );
}