import styled from "styled-components";

const User = styled.View`
  flex: 1;
  height: 100%;
  margin-vertical: 5px;
  margin-horizontal: 5px;
  border-radius: 20px;
`;

const ImgAndName = styled.View`
  flex-direction: row;
  border-radius: 20px;
  align-self: center;
  justify-content: center;
`;

const Name = styled.View`
  flex: 1;
  align-items: center;
  margin-vertical: 10px;
`;

const ImageBack = styled.ImageBackground`
  align-self: center;
  overflow: hidden;
  border-radius: 15px;
  margin-bottom: 10px;
`;

const AllInfo = styled.View`
  flex: 1;
  width: 100%;
  padding-horizontal: 10px;
  border-radius: 20px;
  justify-content: space-evenly;
  border-radius: 20px;
  align-self: center;
`;

const ShareViaEmail = styled.TouchableHighlight`
  position: absolute;
  top: 10px;
  right: 8px;
  align-items: center;
`;

const ShareViaShare = styled.TouchableHighlight`
  position: absolute;
  top: 60px;
  right: 8px;
  align-items: center;
`;

export {
  User,
  ImgAndName,
  Name,
  ImageBack,
  AllInfo,
  ShareViaEmail,
  ShareViaShare,
};
