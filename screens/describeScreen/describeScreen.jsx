import React from "react";
import { View } from "react-native";
import TextComponent from "../../src/shared/components/Text";
import ImageComponent from "../../src/shared/components/Image";
import {
  User,
  ImgAndName,
  Name,
  ImageBack,
  AllInfo,
  ShareViaEmail,
  ShareViaShare,
} from "./describeScreen.styled";
import * as Sharing from "expo-sharing";
import Dialog from "react-native-dialog";
import Communications from "react-native-communications";
import { captureScreen } from "react-native-view-shot";

export default function describeScreen(props) {
  const [visible, setVisible] = React.useState(false);
  const [emailTo, setEmailTo] = React.useState("");
  const [correct, setCorrect] = React.useState(false);
  const [correctColor, setCorrectColor] = React.useState("red");
  const [disable, setDisable] = React.useState(true);
  const [sendColor, setSendColor] = React.useState("grey");

  let {
    theme,
    send: { name },
    send: { picture },
    send: { dob },
    send: { gender },
    send: { location },
    send: { email },
    send: { phone },
  } = props.route.params;

  const text = `Hello, i am ${gender}, i live in ${location.street.name} ${
    location.street.number
  } city ${location.city}, state ${location.state}, in ${
    location.country
  }. I am ${dob.age} age old. My date of birth is ${dob.date.slice(
    0,
    10
  )}. I will be glad to talk.`;

  const handleCancel = () => {
    setVisible(false);
  };
  const handleSend = () => {
    setVisible(false);
    Communications.email([emailTo], null, null, "nunu", text);
  };
  const alertSend = () => {
    setVisible(true);
  };

  React.useEffect(() => {
    const check =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    check.test(emailTo)
      ? (setCorrect("your email is valid"),
        setCorrectColor("blue"),
        setSendColor("#24baff"),
        setDisable(false))
      : (setCorrect("invalid email"),
        setCorrectColor("red"),
        setSendColor("rgba(189, 189, 189, 0.5)"),
        setDisable(true));
  }, [emailTo]);

  const onShare = async () => {
    captureScreen({
      format: "jpg",
      quality: 1.0,
    }).then(
      (uri) => Sharing.shareAsync(uri),
      (error) => console.error("Oops, snapshot failed", error)
    );

    if (!(await Sharing.isAvailableAsync())) {
      alert(`Uh oh, sharing isn't available on your platform`);
      return;
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Dialog.Container visible={visible} onBackdropPress={handleCancel}>
        <Dialog.Title style={{ color: "#24baff" }}>Share card</Dialog.Title>
        <Dialog.Description style={{ color: "#8f8731" }}>
          Write email address below
        </Dialog.Description>
        <Dialog.Input
          placeholder="example@mail.com"
          onChangeText={(text) => setEmailTo(text)}
          value={emailTo}
        />
        <Dialog.Description style={{ color: correctColor, fontSize: 13 }}>
          {correct}
        </Dialog.Description>
        <Dialog.Button
          label="Send"
          color={sendColor}
          onPress={handleSend}
          disabled={disable}
        />
        <Dialog.Button label="Cancel" onPress={handleCancel} color="red" />
      </Dialog.Container>
      <User theme={theme}>
        <ImageBack source={theme.image} style={{ flex: 1 }}>
          <ImgAndName theme={theme}>
            <ImageComponent
              type="imgDescription"
              source={{ uri: picture.large }}
            />
            <Name>
              <TextComponent type="describe" theme={theme} text="Name: " />
              <TextComponent type="nameText" theme={theme} text={name.first} />
              <TextComponent type="describe" theme={theme} text="Last Name: " />
              <TextComponent type="nameText" theme={theme} text={name.last} />
              <TextComponent type="describe" theme={theme} text="Age: " />
              <TextComponent type="nameText" theme={theme} text={dob.age} />
            </Name>
            <ShareViaEmail onPress={() => alertSend()}>
              <ImageComponent
                type="icon"
                source={require("../../assets/shareemail.png")}
              />
            </ShareViaEmail>
            <ShareViaShare onPress={() => onShare()}>
              <ImageComponent
                type="icon"
                source={require("../../assets/share.png")}
              />
            </ShareViaShare>
          </ImgAndName>
        </ImageBack>
        <ImageBack source={theme.image} style={{ flex: 2 }}>
          <AllInfo theme={theme}>
            <TextComponent type="description" theme={theme} text={text} />
            <TextComponent
              type="description"
              color={theme.titleName}
              theme={theme}
              text="email: "
            />
            <TextComponent type="description" theme={theme} text={email} />
            <TextComponent
              type="description"
              color={theme.titleName}
              theme={theme}
              text="phone: "
            />
            <TextComponent type="description" theme={theme} text={phone} />
          </AllInfo>
        </ImageBack>
      </User>
    </View>
  );
}
