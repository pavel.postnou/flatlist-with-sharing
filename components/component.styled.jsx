import styled from "styled-components/native";

const VerticalDevider = styled.View`
  height: 110px;
  width: 1px;
  background-color: orange;
  margin-horizontal:10px;
  align-self:center;
`;

const CardContainer = styled.View`
  height: 125px;
  width: 97%;
  flex-direction: row;
  margin-bottom: 5px;
  margin-left: 7px;
  border-radius: 10px;
`;

const ImageBack = styled.ImageBackground`
  resize-mode: cover;
  width: 95%;
  margin-bottom: 7px;
  align-self: center;
  overflow:hidden;
  border-radius: 15px;
`;

const InfoContainer = styled.View`
  flex: 1;
  justify-content: center;
`;
const ImageContainer = styled.View`
  flex: 1;
  justify-content: center;
`;


export {VerticalDevider, CardContainer, ImageBack, InfoContainer, ImageContainer}