import React from "react";
import * as RootNavigation from "../navigation/RootNavigation";
import TextComponent from "../src/shared/components/Text";
import ImageComponent from "../src/shared/components/Image";
import { TouchableOpacity} from "react-native";
import {
  VerticalDevider,
  CardContainer,
  ImageBack,
  InfoContainer,
  ImageContainer,
} from "./component.styled";

function Item({ data, users, theme }) {
  const goInfo = () => {
    let send;
    users
      ? ((send = users.results[data.number]),
        RootNavigation.navigate("Describe", { send, theme }))
      : null;
  };

  return data ? (
    <TouchableOpacity onPress={() => goInfo()}>
      <ImageBack source={theme.image}>
        <CardContainer theme={theme}>
          <ImageContainer>
            <ImageComponent type="imgOne" source={{ uri: data.image }} />
          </ImageContainer>
          <VerticalDevider></VerticalDevider>
          <InfoContainer>
            <TextComponent theme={theme} type="naming" text="name: " />
            <TextComponent
              color={theme.titleName}
              type="title"
              text={data.name}
            />
            <TextComponent theme={theme} type="naming" text="last name: " />
            <TextComponent
              color={theme.titleName}
              type="title"
              text={data.lastName}
            />
          </InfoContainer>
          <VerticalDevider></VerticalDevider>
          <InfoContainer>
            <TextComponent theme={theme} type="naming" text="country: " />
            <TextComponent
              color={theme.titleCountry}
              type="title"
              text={data.country}
            />
            <TextComponent theme={theme} type="naming" text="city: " />
            <TextComponent
              color={theme.titleCountry}
              type="title"
              text={data.city}
            />
            <TextComponent theme={theme} type="naming" text="registered: " />
            <TextComponent theme={theme} type="date" text={data.date} />
          </InfoContainer>
        </CardContainer>
      </ImageBack>
    </TouchableOpacity>
  ) : null;
}

export default React.memo(Item);
